# OpenJAX DBCP

> Database Connection Pool

[![Build Status](https://travis-ci.org/openjax/dbcp.png)](https://travis-ci.org/openjax/dbcp)
[![Coverage Status](https://coveralls.io/repos/github/openjax/dbcp/badge.svg)](https://coveralls.io/github/openjax/dbcp)
[![Javadocs](https://www.javadoc.io/badge/org.openjax/dbcp.svg)](https://www.javadoc.io/doc/org.openjax/dbcp)
[![Released Version](https://img.shields.io/maven-central/v/org.openjax/dbcp.svg)](https://mvnrepository.com/artifact/org.openjax/dbcp)

### Introduction

**dbcp** is a light wrapper around the [Apache Commons DBCP][apache-commons-dbcp] library, which provides a simple API to describe and initialize a JDBC Database Connection Pool.

### Why **dbcp**?

#### CohesionFirst

Developed with the CohesionFirst approach, **dbcp** is an easy-to-use and simple solution that separates itself from the rest with the strength of its cohesion and ease of usability. Made possible by the rigorous conformance to best practices in every line of its implementation, **dbcp** considers the needs of the developer as primary, and offers a complete solution for the command line arguments facet of an application.

#### Complete Solution

**dbcp** allows a developer to configure a Connection Pool with a [standardized XML Schema][dbcp-schema], which is used by a consumer class to initiate the connection pool. **dbcp** uses the JAXB framework to significantly reduce the boilerplate code, thus providing a lean API with support for the all possible connection pool configuration variations.

#### Validating and Fail-Fast

**dbcp** is based on a [XML Schema][dbcp-schema] used to specify the formal of XML documents accepted by the configuration consumer. The XML Schema is designed to use the full power of XML Validation to allow a developer to qiuckly determine errors in his draft. Once a `dbcp.xml` passes the validation checks, it is almost guaranteed to properly initialize the Connection Pool configured by the file.

### Getting Started

#### Prerequisites

* [Java 8][jdk8-download] - The minimum required JDK version.
* [Maven][maven] - The dependency management system.

#### Example

1. In your preferred development directory, create a [`maven-archetype-quickstart`][maven-archetype-quickstart] project.

    ```bash
    mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
    ```

2. Add the `mvn.repo.openjax.org` Maven repositories to the POM.

    ```xml
    <repositories>
      <repository>
        <id>mvn.repo.openjax.org</id>
        <url>http://mvn.repo.openjax.org/m2</url>
      </repository>
    </repositories>
    <pluginRepositories>
      <pluginRepository>
        <id>mvn.repo.openjax.org</id>
        <url>http://mvn.repo.openjax.org/m2</url>
      </pluginRepository>
    </pluginRepositories>
    ```

3. Create a `dbcp.xml` in `src/main/resources/`.

    ```xml
    <dbcp
      xmlns="http://www.openjax.org/dbcp-1.0.4.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.openjax.org/dbcp-1.0.4.xsd http://www.openjax.org/dbcp.xsd"
      name="basis">
      <jdbc>
        <url>jdbc:postgresql://localhost/basis</url>
        <driverClassName>org.postgresql.Driver</driverClassName>
        <username>basis</username>
        <password>basis</password>
        <loginTimeout>5000</loginTimeout>
      </jdbc>
      <default>
        <autoCommit>true</autoCommit>
        <readOnly>false</readOnly>
        <transactionIsolation>READ_UNCOMMITTED</transactionIsolation>
      </default>
      <size>
        <initialSize>0</initialSize>
        <maxActive>16</maxActive>
        <maxIdle>16</maxIdle>
        <minIdle>0</minIdle>
        <maxWait>1000</maxWait>
      </size>
      <management>
        <timeBetweenEvictionRuns>-1</timeBetweenEvictionRuns>
        <numTestsPerEvictionRun>3</numTestsPerEvictionRun>
        <minEvictableIdleTime>1800000</minEvictableIdleTime>
      </management>
      <preparedStatements>
        <poolPreparedStatements>false</poolPreparedStatements>
        <maxOpenPreparedStatements>-1</maxOpenPreparedStatements>
      </preparedStatements>
      <removal>
        <removeAbandoned>false</removeAbandoned>
        <removeAbandonedTimeout>300</removeAbandonedTimeout>
        <logAbandoned>false</logAbandoned>
      </removal>
      <logging>
        <level>ALL</level>
        <logExpiredConnections>true</logExpiredConnections>
        <logAbandoned>true</logAbandoned>
      </logging>
    </dbcp>
    ```

4. Add `org.openjax:dbcp` dependency to the POM.

    ```xml
    <dependency>
      <groupId>org.openjax</groupId>
      <artifactId>dbcp</artifactId>
      <version>1.0.4-SNAPSHOT</version>
    </dependency>
    ```

5. In the `main()` method in `App.java`, add the following line and let your IDE resolve the missing imports.

    ```java
    DataSource dataSource = DataSources.createDataSource(Thread.currentThread().getContextClassLoader().getResource("dbcp.xml"));
    ```

    The `dataSource` object is a reference to the initialized JDBC Connection Pool configured in `dbcp.xml`.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.

[apache-commons-dbcp]: https://commons.apache.org/proper/commons-dbcp
[dbcp-schema]: /src/main/resources/dbcp.xsd
[jdk8-download]: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
[maven-archetype-quickstart]: http://maven.apache.org/archetypes/maven-archetype-quickstart/
[maven]: https://maven.apache.org/